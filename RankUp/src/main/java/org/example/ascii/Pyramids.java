package org.example.ascii;

public class Pyramids {

    /**
     * * * * * * * * *
     *   * * * * * * *
     *     * * * * * *
     *       * * * * *
     *         * * * *
     *           * * *
     *             * *
     *               *
     */
    public static void printHalfPyramid() {
        int counterStar = 8;

        for(int i = 0; i < 8; i++) {
            for(int k = 0; k < i; k++){
                System.out.print("  ");
            }
            for(int j = 0; j < counterStar; j++) {
                System.out.print("# ");
            }
            counterStar--;
            System.out.println();
        }
    }
}
