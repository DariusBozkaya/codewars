package org.example.kyu6;

import java.util.*;

/**
 * Task
 *
 * Given a list and a number, create a new list that contains each number of list at most N times, without reordering.
 * For example if the input number is 2, and the input list is [1,2,3,1,2,1,2,3], you take [1,2,3,1,2],
 * drop the next [1,2] since this would lead to 1 and 2 being in the result 3 times, and then take 3, which leads to [1,2,3,1,2,3].
 * With list [20,37,20,21] and number 1, the result would be [20,37,21].
 */

public class DeleteNth {

    public static int[] deleteNth(int[] elements, int maxOccurrences) {

        //intermediary list used to add numbers for output, while iterating over original list
        List<Integer> numberOutList = new LinkedList<>();

        // used to map numbers to their occurrences
        // key: number, value: occurrence
        HashMap<Integer,Integer> counterMap = new HashMap<>();

        //for (int i = 0; i < elements.length; i++) {
        for (int element : elements) {
            //I had to use this outer if. Otherwise, Exception was thrown because counterMap.get(elements[i]) was null
            //at first iteration
            //TODO getOrDefault instead of outer if?
            if (counterMap.containsKey(element)) {
                //add 1 to value = occurrences
                counterMap.put(element,counterMap.get(element) + 1);

                //adds the number(key) to the output list unless its maxOccurrence(value) is not exceeded
                if(counterMap.get(element) <= maxOccurrences ){
                    numberOutList.add(element);
                }
            }
            //letter not in HashMap
            else {
                //save to hash map
                counterMap.put(element, 1);
                //since it is the first occurrence, definitely save to output list
                numberOutList.add(element);
            }
        }
        //System.out.println(numberOutList.size());
        //Exercise requires int[] as output -
        if (maxOccurrences !=0) {
            //int[] numberOut = new int[numberOutList.size()];
            //casts from Integer to int and fills the array
            int[] numberOut = numberOutList.stream().mapToInt(Integer::intValue).toArray();
            System.out.println(Arrays.toString(numberOut));
            return numberOut;
        }
        return new int [] {};
    }
}
