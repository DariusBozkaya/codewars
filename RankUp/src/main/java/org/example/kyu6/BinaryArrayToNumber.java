package org.example.kyu6;

import java.util.List;

/**
 * Ones and Zeros
 *
 * Given an array of ones and zeroes, convert the equivalent binary value to an integer.
 *
 * Eg: [0, 0, 0, 1] is treated as 0001 which is the binary representation of 1.
 *
 * Examples:
 *
 * Testing: [0, 0, 0, 1] ==> 1
 * Testing: [0, 0, 1, 0] ==> 2
 * Testing: [0, 1, 0, 1] ==> 5
 * Testing: [1, 0, 0, 1] ==> 9
 * Testing: [0, 0, 1, 0] ==> 2
 * Testing: [0, 1, 1, 0] ==> 6
 * Testing: [1, 1, 1, 1] ==> 15
 * Testing: [1, 0, 1, 1] ==> 11
 *           8  4  2  1
 * However, the arrays can have varying lengths, not just limited to 4.
 */
public class BinaryArrayToNumber {

    public static int convertBinaryArrayToInt(List<Integer> binary) {
        // size of list is needed since decimal representation of binary digit at first index is:
        // 2^(list size - 1) e.g. [1, 0, 1, 1] -> 2^3 + 2^2 + 2^1 + 2^0
        int listSize = binary.size();
        int number = 0;
        for (Integer integer : binary) {
            if (integer == 1) {
                number += Math.pow(2, (listSize - 1));
            }
            listSize--;
        }
        return number;
    }
}
