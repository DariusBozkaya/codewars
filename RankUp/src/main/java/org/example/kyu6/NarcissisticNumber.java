package org.example.kyu6;

/**
 * A Narcissistic Number (or Armstrong Number) is a positive number which is the sum of its own digits,
 * each raised to the power of the number of digits in a given base.
 * In this Kata, we will restrict ourselves to decimal (base 10).
 *
 * For example, take 153 (3 digits), which is narcissistic:
 *
 *     1^3 + 5^3 + 3^3 = 1 + 125 + 27 = 153
 *
 * and 1652 (4 digits), which isn't:
 *
 *     1^4 + 6^4 + 5^4 + 2^4 = 1 + 1296 + 625 + 16 = 1938
 *
 * The Challenge:
 * Your code must return true or false (not 'true' and 'false') depending upon whether the given number
 * is a Narcissistic number in base 10.
 * This may be True and False in your language, e.g. PHP.
 * Error checking for text strings or other invalid inputs is not required, only valid positive non-zero integers
 * will be passed into the function.
 */
public class NarcissisticNumber {
    public static boolean isArmstrongNumber(int number){
        //convert number to string for iteration
        String numberStr = String.valueOf(number);
        int sum = 0;
        int numberDigits = getNumberDigits(number);

        for(int i = 0; i < numberStr.length(); i++) {
            //each digit is raised to the power of number of digits the number has
            //and added to the sum
            sum += Math.pow(Character.getNumericValue(numberStr.charAt(i)), numberDigits);;
        }
        boolean isArmstrong;
        return isArmstrong = sum == number ? true : false;

    }


    /**
     * There are multiple ways to get the number of digits of an integer value
     * String approach (slow): int numberDigits = String.valueOf(n).length();
     * Here: logarithmic approach (faster): log base 10 and then round up -
     * For negative numbers: first Math.abs(number) then log10
     * @param n
     * @return
     */
    public static int getNumberDigits(int n) {
        //String approach
        //numberDigits = String.valueOf(n).length();

        //logarithmic approach by getting log10 and rounding up
        //round up: e.g. 2.3 --> 2.3 + 1 = 3.3 --> cast to int = 3 eliminates decimal point
        int numberDigits = (int) (Math.log10(n) + 1);

        //rounding up with Math.ceil
        //int numberDigits = (int) Math.ceil(Math.log10(n));

        return numberDigits;
    }
}
