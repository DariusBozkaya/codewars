package org.example.kyu6;


/**
 * This is a Kata from the Rank Up Tier of codewars.com
 *
 * You probably know the "like" system from Facebook and other pages. People can "like" blog posts,
 * pictures or other items. We want to create the text that should be displayed next to such an item.
 *
 * Implement the function which takes an array containing the names of people that like an item.
 * It must return the display text as shown in the examples:
 *
 * []                                -->  "no one likes this"
 * ["Peter"]                         -->  "Peter likes this"
 * ["Jacob", "Alex"]                 -->  "Jacob and Alex like this"
 * ["Max", "John", "Mark"]           -->  "Max, John and Mark like this"
 * ["Alex", "Jacob", "Mark", "Max"]  -->  "Alex, Jacob and 2 others like this"
 *
 * Note: For 4 or more names, the number in "and 2 others" simply increases.
 */
public class WhoLikes {
    public static String whoLikesIt(String... names) {
        String output;
        switch (names.length) {
            case 0:
                output = "no one likes this";
                break;
            case 1:
                output = String.format("%s likes this", names[0]);
                break;
            case 2:
                output = String.format("%s and %s like this", names[0],names[1]);
                break;
            case 3:
                output = String.format("%s, %s and %s like this", names[0],names[1],names[2]);
                break;
            //matches if 4 or more people like this
            default:
                output = String.format("%s, %s and %s others like this", names[0],names[1],(names.length-2));
        }
        return output;
    }
}
