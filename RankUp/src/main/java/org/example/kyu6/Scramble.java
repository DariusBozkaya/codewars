package org.example.kyu6;

import java.util.HashMap;

/**
 * Complete the function scramble(str1, str2) that returns true if a portion of str1 characters can be rearranged
 * to match str2, otherwise returns false.
 *
 * Notes:
 *
 *     Only lower case letters will be used (a-z). No punctuation or digits will be included.
 *     Performance needs to be considered.
 *
 * Examples
 *
 * scramble('rkqodlw', 'world') ==> True
 * scramble('cedewaraaossoqqyt', 'codewars') ==> True
 * scramble('katas', 'steak') ==> False
 */
public class Scramble {

    public static boolean scramble(String str1, String str2) {

        boolean contains = false;
        //The scrambled word (because longer) is mapped by key = letter and value = occurrence
        HashMap<String,Integer> map = new HashMap<>();
        for (int i = 0; i < str2.length(); i++) {
            if(map.containsKey(String.valueOf(str2.charAt(i)))){
                map.put(String.valueOf(str2.charAt(i)), map.get(String.valueOf(str2.charAt(i))) + 1 );
            } else {
                map.put(String.valueOf(str2.charAt(i)),1);
            }
        }
        //Iterates through the word and checks if char is found in keys. If yes, subtracts 1 from value.
        //When key is not found, for is broken
        for(int i = 0; i < str1.length(); i++) {
            if(map.containsKey(String.valueOf(str1.charAt(i))) && map.get(String.valueOf(str1.charAt(i))) != 0) {
                map.put(String.valueOf(str1.charAt(i)), map.get(String.valueOf(str1.charAt(i))) - 1 );
                contains = true;
            }
            else {
                contains = false;
                break;
            }
        }
        return contains;
    }

    /**
     * Maps a string to a hash map.
     * key: letter, value: number of occurrences;
     * @param word The string that will be mapped.
     * @return A HashMap with letters as keys and their number of occurrences as value of a given string.
     */
    public static HashMap<String,Integer> mapString(String word) {
        HashMap<String,Integer> map = new HashMap<>();
        for (int i = 0; i < word.length(); i++) {
            if(map.containsKey(String.valueOf(word.charAt(i)))){
                map.put(String.valueOf(word.charAt(i)), map.get(String.valueOf(word.charAt(i))) + 1 );
            } else {
                map.put(String.valueOf(word.charAt(i)),1);
            }
        }
        //System.out.println(map);
        return map;
    }
}
