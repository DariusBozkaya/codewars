package org.example;

import org.example.ascii.Pyramids;
import org.example.kyu6.BinaryArrayToNumber;
import org.example.kyu6.DeleteNth;
import org.example.kyu6.Scramble;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        //Who likes?
        //String[] names = {"Maria", "Peter", "Andrea", "Slobodan", "Christo"};
        //WhoLikes.whoLikesIt(names);

        //NarcissisticNumber.getNumberDigits(123443345);
        //NarcissisticNumber.isArmstrongNumber(123);

        int n = 2;
        int[] arr = {1,2,3,1,1,2,3,1,2,4,4,4,5,5};
        //int[] arr = {};
        DeleteNth.deleteNth(arr, n);

        /*
        long startTime = System.nanoTime();
        String str1 = "codewars";
        String str2 = "cedewaraaossoqqyt";
        Scramble.scramble(str1,str2);
        long endTime = System.nanoTime();
        System.out.println((endTime - startTime));
        */
        //Scramble.mapString("cedewaraaossoqqyt");

        //List<Integer> binaryList = Arrays.asList(1,0,0,1);
        //BinaryArrayToNumber.convertBinaryArrayToInt(binaryList);
        //Pyramids.printHalfPyramid();
    }
}