package org.example.kyu6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WhoLikesTest {

    @Test
    void whoLikesIt() {
        assertEquals("no one likes this", WhoLikes.whoLikesIt());
        assertEquals("Alex likes this", WhoLikes.whoLikesIt("Alex"));
        assertEquals("Alex and Maria like this", WhoLikes.whoLikesIt("Alex", "Maria"));
        assertEquals("Alex, Maria and Mo like this", WhoLikes.whoLikesIt("Alex","Maria","Mo"));
        assertEquals("Alex, Maria and 2 others like this", WhoLikes.whoLikesIt("Alex","Maria","Mo","Mike"));
    }
}