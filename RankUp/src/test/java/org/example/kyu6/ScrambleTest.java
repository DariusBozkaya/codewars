package org.example.kyu6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ScrambleTest {

    @Test
    void scramble() {
        assertTrue(Scramble.scramble("codewars", "cedewaraaossoqqyt"));
        assertTrue(Scramble.scramble("javascript", "scriptingjava"));
        assertFalse(Scramble.scramble("katas", "steak"));
    }
}