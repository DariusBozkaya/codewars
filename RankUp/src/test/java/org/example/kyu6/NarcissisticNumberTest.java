package org.example.kyu6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NarcissisticNumberTest {

    @Test
    void isArmstrongNumber() {


        assertTrue(NarcissisticNumber.isArmstrongNumber(153), "153 is a Armstrong number");
        assertTrue(NarcissisticNumber.isArmstrongNumber(370));
        assertFalse(NarcissisticNumber.isArmstrongNumber(112));
    }
}