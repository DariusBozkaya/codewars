package org.example.kyu6;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BinaryArrayToNumberTest {

    @Test
    void convertBinaryArrayToInt() {
        assertEquals(19, BinaryArrayToNumber.convertBinaryArrayToInt(new ArrayList<>(Arrays.asList(1,0,0,1,1))));
        assertEquals(7, BinaryArrayToNumber.convertBinaryArrayToInt(new ArrayList<>(Arrays.asList(0,1,1,1))));
        assertEquals(1, BinaryArrayToNumber.convertBinaryArrayToInt(new ArrayList<>(List.of(1))));

    }
}