package org.example.kyu6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeleteNthTest {

    @Test
    void deleteNth() {
        assertArrayEquals(new int[] { 20, 37, 21 },DeleteNth.deleteNth(new int[] { 20, 37, 20, 21 }, 1));
        assertArrayEquals(new int[] { 1, 1, 3, 3, 7, 2, 2, 2 },DeleteNth.deleteNth(new int[] { 1, 1, 3, 3, 7, 2, 2, 2, 2 }, 3));
        assertArrayEquals(new int[] { },DeleteNth.deleteNth(new int[] { }, 3));
    }
}