# Codewars challenges

## Description
[Codewars](https://www.codewars.com) is a platform that helps improving coding skills.
Programming tasks, called "katas" are provided for different programming languages. 

Here I solve katas in Java.
After submitting my own solution, I enjoy studying other users solutions. This way I learn new angles on problem solving as well as java features and programming concepts I am less familiar with.

 

